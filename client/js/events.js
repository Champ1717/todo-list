"use strict";

Template.form.events({
	'submit .add-new-task': function (event) {
		event.preventDefault();

		var taskName = event.currentTarget.children[0].value;
		Collections.Todo.insert({
			name: taskName,
			createdAt: new Date(),
			complete: false
		});

		return false;
	}

});